import click

import time

from kim.services import svc_check


class Context:
    def __init__(self, directory):
        self.directory = directory
        self.check = svc_check.Check()


@click.group()
@click.option("-d", "--directory", type=str, help="Checks for this farm")
@click.pass_context
def cli(ctx, directory):
    """Check Pula Farm Data"""
    ctx.obj = Context(directory)


@cli.command()
@click.pass_context
def checkall(ctx):
    """Data at the directory"""
    ctx.obj.check.runchecks(directory = ctx.obj.directory)
