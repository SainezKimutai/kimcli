import click

import os, json

from kim.config import BASE_PATH, DEFAULT_DATA_PATH


@click.group()
def cli():
    """ Pula Farm assessment """
    pass

@cli.command()
def loadData():
    file_path = BASE_PATH + DEFAULT_DATA_PATH
    with open(file_path) as file:
        data = json.load(file)
        print(data)

@cli.command()
def showData():
    pass
