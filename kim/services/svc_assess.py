# import os, json
# import pandas as pd
#
#
# class Assess:
#
#     @classmethod
#     def upload_data(cls):
#         # this finds our json files
#         path_to_json = '../assets/harvest_data_set'
#         json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
#
#         # here I define my pandas Dataframe with the columns I want to get from the json
#         jsons_data = pd.DataFrame(columns=['farm_id', 'crop', 'location', 'wet_weight', 'dry_weight'])
#
#         # we need both the json and an index number so use enumerate()
#         for index, js in enumerate(json_files):
#             with open(os.path.join(path_to_json, js)) as json_file:
#                 json_text = json.load(json_file)
#
#                 # here you need to know the layout of your json and each json has to have
#                 # the same structure (obviously not the structure I have here)
#                 farm_id = json_text['harvest_measurements']['farm_id']
#                 crop = json_text['harvest_measurements']['crop']
#                 location = json_text['harvest_measurements']['location']
#                 wet_weight = json_text['harvest_measurements']['wet_weight']
#                 dry_weight = json_text['harvest_measurements']['dry_weight']
#                 # here I push a list of data into a pandas DataFrame at row given by 'index'
#                 jsons_data.loc[index] = [farm_id, crop, location, wet_weight, dry_weight]
#
#         # now that we have the pertinent json data in our DataFrame let's look at it
#         return(jsons_data)
